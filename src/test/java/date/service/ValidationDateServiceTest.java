package date.service;

import date.exception.InvalidDateFormatException;
import date.exception.UnknownDateFormatException;
import org.junit.Test;

import java.text.SimpleDateFormat;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.*;


public class ValidationDateServiceTest {

    final static String DATE_FORMAT = "yyyy-MM-dd";

    @Test
    public void shouldReturnDate() throws Exception {
        //given
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT);
        ValidationDateService validationDateService = new ValidationDateService();
        String date = "2020-10-10";
        //when
        String expected = "2020-10-10";
        validationDateService.validate(date);
        //then - no exception
    }

    @Test(expected = InvalidDateFormatException.class)
    public void shouldCatchEmptyString() throws Exception {
        //given
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT);
        ValidationDateService validationDateService = new ValidationDateService();
        String date = "";
        //when
        String expected = date;
        validationDateService.validate(date);
        //then - no exception

    }

    @Test(expected = InvalidDateFormatException.class)
    public void shouldCatchNull() throws Exception {
        //given
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT);
        ValidationDateService validationDateService = new ValidationDateService();
        String date = null;
        //when
        String expected = date;
        validationDateService.validate(date);
    }

    @Test(expected = InvalidDateFormatException.class)
    public void shouldReturnException() throws Exception {
        //given
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT);
        ValidationDateService validationDateService = new ValidationDateService();
        String date = "asd";
        //when
        String expected = "2019-10-10";
        validationDateService.validate(date);
    }

    @Test(expected = InvalidDateFormatException.class)
    public void shouldReturnExcept() throws Exception {
        //given
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT);
        ValidationDateService validationDateService = new ValidationDateService();
        String date = "10-10-201";
        //when
        String expected = "2019-10-10";
        validationDateService.validate(date);
        //then - no exception
    }

    @Test(expected = UnknownDateFormatException.class)
    public void shouldReturnUnknownDateFormat() throws Exception {
        //given
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT);
        ValidationDateService validationDateService = new ValidationDateService();
        String date = "2019-14-01";
        //when
        String expected = date;
        validationDateService.validate(date);
    }

    @Test
    public void shouldReturnCorrectPatternFromUser() {
        //given
        ValidationDateService validationDateService = new ValidationDateService();
        //when
        String expected = "2020-10-10";
        String result = validationDateService.validateDate("2020-10-10", "2020-10-10");
        //then
        assertEquals(expected, result);
    }

    @Test(expected = InvalidDateFormatException.class)
    public void shouldReturnWrongDateFormat() throws Exception {
        //given
        ValidationDateService validationDateService = new ValidationDateService();
        //when
        String expected = "2020-10-10";
        String result = validationDateService.validateDate("10-10-2020", "2020-10-10");
        //then
        assertEquals(expected, result);
    }

    @Test(expected = UnknownDateFormatException.class)
    public void shouldReturnUnknownDate() throws Exception {
        //given
        ValidationDateService validationDateService = new ValidationDateService();
        //when
        String expected = "2020-10-10";
        String result = validationDateService.validateDate("2019-14-01", "2020-10-10");
        //then
        assertEquals(expected, result);
    }

    @Test(expected = InvalidDateFormatException.class)
    public void shouldReturnNullValue() {
        ValidationDateService validationDateService = new ValidationDateService();
        assertFalse(validationDateService.isThisDateValid(null, "yyyy-MM-dd"));
    }

    @Test(expected = InvalidDateFormatException.class)
    public void shouldReturnEmptyValue() {
        ValidationDateService validationDateService = new ValidationDateService();
        assertFalse(validationDateService.isThisDateValid("", "yyyy-MM-dd"));
    }


    @Test(expected = UnknownDateFormatException.class)
    public void shouldReturnInvalidDay() {
        ValidationDateService validationDateService = new ValidationDateService();
        assertFalse(validationDateService.isThisDateValid("2020-10-32", "yyyy-MM-dd"));
    }

    @Test(expected = InvalidDateFormatException.class)
    public void shouldReturnInvalidFormat() {
        ValidationDateService validationDateService = new ValidationDateService();
        assertFalse(validationDateService.isThisDateValid("10-10-2020", "yyyy-MM-dd"));
    }

    @Test(expected = UnknownDateFormatException.class)
    public void shouldReturnUnknownFormat() {
        ValidationDateService validationDateService = new ValidationDateService();
        assertFalse(validationDateService.isThisDateValid("2020-10-32", "yyyy-MM-dd"));
    }

    @Test(expected = UnknownDateFormatException.class)
    public void shouldReturnWrongDate() {
        ValidationDateService validationDateService = new ValidationDateService();
        assertFalse(validationDateService.isThisDateValid("2020-40-40", "yyyy-MM-dd"));
    }

    @Test(expected = UnknownDateFormatException.class)
    public void shouldReturnWrongYear() {
        ValidationDateService validationDateService = new ValidationDateService();
        assertFalse(validationDateService.isThisDateValid("2020-14-10", "yyyy-MM-dd"));
    }



}