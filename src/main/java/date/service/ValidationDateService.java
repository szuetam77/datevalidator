package date.service;

import date.exception.InvalidDateFormatException;
import date.exception.UnknownDateFormatException;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ValidationDateService {

    final static String DATE_FORMAT = "yyyy-MM-dd";


    public void validate(String date) {

        if (date == null || !date.matches("\\d{4}[-]\\d{2}[-]\\d{2}") || date == "") {
            throw new InvalidDateFormatException();
        }
        try {
            DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
            dateFormat.setLenient(false);
            dateFormat.parse(date);
        } catch (ParseException e) {
            throw new UnknownDateFormatException();
        }
    }

    public String validateDate(String dateFromUser, String DATE_FORMAT) {
        SimpleDateFormat fromUser = new SimpleDateFormat(dateFromUser);
        SimpleDateFormat approvedFormat = new SimpleDateFormat(DATE_FORMAT);

        if (dateFromUser == null || !dateFromUser.matches("\\d{4}[-]\\d{2}[-]\\d{2}") || dateFromUser == "") {
            throw new InvalidDateFormatException();
        }

        try {
            String reformattedDate = approvedFormat.format(fromUser.parse(DATE_FORMAT));
        } catch (ParseException e) {
            throw new UnknownDateFormatException();
        }
        return DATE_FORMAT;
    }

    public boolean isThisDateValid(String dateToValidate, String dateFormat) {

        if (dateToValidate == null || !dateToValidate.matches("\\d{4}[-]\\d{2}[-]\\d{2}") || dateToValidate == "") {
            throw new InvalidDateFormatException();
        }

        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        sdf.setLenient(false);

        try {
            Date date = sdf.parse(dateToValidate);
            System.out.println(date);
        } catch (ParseException e) {
            throw new UnknownDateFormatException();
        }
        return true;
    }

}


























// ///////////////////////////////// INVALID METHODS /////////////////////////////////

//    public String validateDate(String date){
//
//
//
//        if(date == null || date == "") {
//            throw new InvalidDateFormatException();
//        }
//
//        try{
//            Pattern pattern = Pattern.compile(DATE_FORMAT);
//
//        }
//
//        return date;
//    }


//    public String validate(String input) {
//
//        // jeśli 'Input' jest nullem/ nie pasuje do regexa/jest pusty -> rzuć wyjątkiem
//        // "NIEPOPRAWNY FORMAT"
//
//        if (input == null || !input.matches("\\d{4}[-]\\d{2}[-]\\d{2}") || input == "") {
//            throw new InvalidDateFormatException();
//        }
//
//        // określeniu patternu daty, który nas interesuje w metodzie
//        String pattern = "yyyy-MM-dd";
//        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
//        simpleDateFormat.setLenient(false);
//
//        // sparsuj datę na format z inputa
//        try {
//            Date date = simpleDateFormat.parse(input);
//            // jeśli format jest niepoprawny -> rzuć wyjątkiem
//            // "NIEZNANY FORMAT"
//        } catch (ParseException e){
//            throw new UnknownDateFormatException();
//        }
//    return input;
//    }


//    private final static String DATE_FORMAT =("yyyy-MM-dd");
//
//    public void validate(String input) {
//        if (input == null || !input.matches("\\d{4}[-]\\d{2}[-]\\d{2}") || input == "") {
//            throw new InvalidDateFormatException();
//        }
//
//        DateFormat df = new SimpleDateFormat(DATE_FORMAT);
//        df.setLenient(false);
//
//        try {
//            df.parse(input);
//
//        }catch (ParseException e) {
//            throw new UnknownDateFormatException();
//        }
//    }


// czym sie rozni runtime exception od exception